import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Navigate, Link, useNavigate} from 'react-router-dom';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

//imports sweetalert
import Swal2 from 'sweetalert2';

export default function Login(){
	const [email, setEmail] = useState('');
	const [Password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

  const navigate = useNavigate();
	//we are going to consume or use the UserContext
	const {user, setUser} = useContext(UserContext);

	//get item method gets the value of the specified key from our local storage
	// const [user, setUser] = useState(localStorage.getItem('email'));

	const retrieveUserdetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {method: 'GET',
			headers:{
				Authorization: `Bearer ${token}`
			}
	  })
	  .then(result => result.json())
	  .then(data => {
	  	console.log(data);
	  	setUser({
	  		id : data._id,
	  		isAdmin: data.isAdmin
	  	});
	  })
	}

	useEffect(() => {
		if (email !== '' && Password !== "" && Password.length > 6) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, Password]);

	function login(e){
		e.preventDefault()

		//process a fetch request to the corresponding backend API
		//Syntax:
		   //fetch(url, {options});

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: Password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title: 'Authorization failed!',
					icon: 'error',
					text: 'Check your login details and try again!'
				})
			} else {
	
				localStorage.setItem('token', data.auth);

				retrieveUserdetails(data.auth);

				Swal2.fire({
					title : 'Login successfully',
					icon : 'success',
					text : 'Welcome to Zuitt!'
				})

				 navigate('/')
			}
		})

		// alert('You are now successfully logged in.')

		//Set the email of the authenticated user in the local storage
		//Syntax:
		   //localStorage.setItem('propertyName', value)

		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'));

		// navigate("/")



		// setEmail('');
	  // setPassword('');
	  // setIsDisabled(true)
	}

	return (

          <Container className = "mt-5">
          	<Row>
          		<Col className = "col-6 mx-auto">
          		   <h1 className = "text-center">Login</h1>
          		   <Form onSubmit = {event => login(event)}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" value = {email} onChange = {event => setEmail(event.target.value)}placeholder="Enter email" />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" value = {Password}
        onChange = {event => setPassword(event.target.value)} placeholder="Password" />
      </Form.Group>
       
       <p>No account yet? <Link to ='/register'>Sign up here</Link></p>

      <Button variant="primary" type="submit" disabled = {isDisabled}>
        Submit
      </Button>
    </Form>
          		</Col>
          	</Row>
          </Container> 
          
		)
}