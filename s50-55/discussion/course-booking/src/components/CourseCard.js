import {Container, Row, Col, Card, Button} from 'react-bootstrap';

import UserContext from '../UserContext.js';

//import the useState hook from react
import {useState, useEffect, useContext} from 'react';

import {Link} from 'react-router-dom';


export default function CourseCard(props){
    //consume the content of the UserContext.
    const {user} = useContext(UserContext);

    console.log(props.courseProp);
    
    // console.log(props);
    // console.log(props.courseProp);

    //object destructuring

    const {_id, name, description, price} = props.courseProp;

    //Use the state hook for this component to be able to store its state
    //states are use to keep track of information related to individual components

         //syntax: const [getter, setter] = useState(initiaGetterValue);

    const [count, setCount] = useState(0);
    //console.log(count);
    const [isdisabled, setIsDisabled] = useState(false);
    const [seat, setSeat] = useState(30);
    //console.log(seat);
    /*setCount(2);
    console.log(count);*/


    //this function will be invoke when the button enroll is clicked

    function enroll(){
       if (seat !== 0) {
        setCount(count + 1);
        setSeat(seat - 1);
        
       } else {
        setIsDisabled(true);
        alert('no more seats!');
       }
       
    }
        
    // console.log(id);  
    
    //the function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are change/changes on our dependencies.
    useEffect(()=> {
        // console.log('hi from useEffect!');
        if(seat === 0){
            //it will state or value of isDisabled to true
            setIsDisabled(true);
        }

    }, [seat]);
    
	return (
        
        <Container className= 'mt-3'>
            <Row>

                <Col className= "mt-3"> 
                   <Card className = "cardHighlight">

                        <Card.Body>
                          <Card.Title>{name}</Card.Title>
                          <Card.Text>
                          <strong>Description</strong>:
                            {description}
                          </Card.Text>
                          <Card.Text>
                          <strong>Price</strong>:
                            {price}
                          </Card.Text>
                          <Card.Subtitle>Enrollees:</Card.Subtitle>
                          <Card.Text>{count}</Card.Text>

                          {
                            user !== null ?

                            <Button as = {Link} to = {`/courses/${_id}`}>Details</Button>

                            :

                            <Button as = {Link} to = '/login'>Login to email</Button>

                          }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>

		)
}