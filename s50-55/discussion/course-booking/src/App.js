
import './App.css';

import {useState, useEffect} from 'react';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js'
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import CourseView from './pages/CourseView.js';

//import UserProdivider
import {UserProvider} from './UserContext.js';

  /* The following line can be included in your src/index.js or App.js file */

import 'bootstrap/dist/css/bootstrap.min.css';

//The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
//The Routes component holds all our Route components. it selects which 'Route' component to show based on the url endpoint.
import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const setLogin = () => {

    if (user.id === null) {
    
    setIsLoggedIn(false)

  } else {

    setIsLoggedIn(true)

  }
}

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage.getItem);
  }, [user]);


  useEffect(() => {
    if(localStorage.getItem('token')){
       fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, { method: 'GET',
        headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      console.log(data);
        setUser({
          id : data._id,
          isAdmin: data.isAdmin
      });
    })
  }  
    }, [])

  return (
     <UserProvider value = {{user, setUser, unsetUser, isLoggedIn, setIsLoggedIn, setLogin}}>
    <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path = "/" element = {<Home/>} />

          <Route path = "/courses" element = {<Courses/>} />

          <Route path = "/register" element = {!isLoggedIn ?  <Register/> : <PageNotFound/>} />

          <Route path = "/login" element = {!isLoggedIn ? <Login/> : <PageNotFound/> } />

          <Route path = "/logout" element = {<Logout/>} />
          <Route path = "/courses/:courseId" element = {<CourseView/>} />

          <Route path = "*" element = {<PageNotFound/>} />
          
        </Routes>
    </BrowserRouter>
  </UserProvider>

   );
}



export default App;
