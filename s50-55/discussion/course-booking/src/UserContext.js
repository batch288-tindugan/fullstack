import React from 'react';

//Create a Context Object
// A Contect Object as the same states is data that can be used to store information that can be shared to other component/s within the app.

//The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop passing.

//With the help of a createContext() method we were able to create a context stored in variable UserContext.
const UserContext = React.createContext();

// 'Provider' component allows other components to consume/ use the context object and supply necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;